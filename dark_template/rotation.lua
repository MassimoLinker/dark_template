local dark_addon = dark_interface

-- replace dark_addon.rotation.spellbooks.class
-- with the appropriate value for your class
-- ex: dark_addon.rotation.spellbooks.mage
local SB = dark_addon.rotation.spellbooks.class

local function combat()
  -- some quick sanity checks before we pew pew
  if not target.alive or not target.enemy or player.channeling then return end

  -- your combat rotation here!
end

local function resting()
  -- your resting rotation here!
end

-- This is what actually tells DR about your custom rotation
dark_addon.rotation.register({
  -- replace dark_addon.rotation.spellbooks.class
  -- with the appropriate value for your class
  -- ex: dark_addon.rotation.classes.mage.frost
  spec = dark_addon.rotation.classes.class.spec,
  -- replace my_rotation with a unique name for your rotation
  -- this is how you will load it in-game, no special characters or spaces!
  -- ex: /dr load my_rotation
  name = 'my_rotation',
  -- This is the textual name of your rotation and acts only
  -- to provide a person friendly name in the rotations list
  label = 'My Awesome Rotation',
  -- You can leave these alone unless you know what you are doing!
  combat = combat,
  resting = resting
})
