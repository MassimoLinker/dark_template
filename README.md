# Dark Template

A DR rotation template that allows sharing rotations in a clean and friendly manner.

## File Names

Feel free to rename the template to anything you wish, however, keep in mind that the name of the folder (in this example, `dark_template`) and the name of the `.toc` file (in this example, `dark_template.toc`), **must match**!

## TOC File

You can make changes to the `.toc` file, the contents are self explanatory, if you don't understand what it might do, don't change it.
